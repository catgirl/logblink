# LogBlink #

Tiny linux daemon that monitors web server access logs for new requests and sends packets to my computer, telling it to blink keyboard light.

Inspired by [this blog post](http://lelandbatey.com/posts/2016/12/Making-lights-blink-for-each-HTTP-request/).

Includes linux server and windows client.