#include <sys/inotify.h>
#include <limits.h>

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))
#define MESSAGE "LOGBLINK"

int main(int argc, char *argv[])
{
	char* sendaddr_str = getenv("LB_SENDADDR");
	char* sendport_str = getenv("LB_SENDPORT");

	if(!sendaddr_str || !sendport_str) {
		puts("Invalid address or port!\n");
		exit(-1);
	}
	int sendport = atoi(sendport_str);
	if(sendport <= 0 || sendport > 65535) {
		puts("Invalid port number!\n");
		exit(-2);
	}

	int inotifyFd, wd, j;
	int sockfd;
	struct sockaddr_in sendaddr;

	char buf[BUF_LEN] __attribute__ ((aligned(8)));
	ssize_t numRead, numSent;
	char *p;
	struct inotify_event *event;

	if (argc < 2 || strcmp(argv[1], "--help") == 0) {
		printf("%s pathname...\n", argv[0]);
		return 1;
	}

	inotifyFd = inotify_init();
	if (inotifyFd == -1) {
		perror("inotify_init");
		exit(1);
	}

	for (j = 1; j < argc; j++) {
		wd = inotify_add_watch(inotifyFd, argv[j], IN_MODIFY);
		if (wd == -1) {
			perror("inoify_add_watch");
			exit(2);
		}
		printf("Watching %s...\n", argv[j]);
	}

	sockfd = socket(PF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0) {
		perror("sockfd");
		exit(3);
	}

	memset(&sendaddr, 0 , sizeof(sendaddr));
	sendaddr.sin_family = AF_INET;
	if(inet_pton(AF_INET, sendaddr_str, &sendaddr.sin_addr) != 1) {
		perror("inet_pton");
		exit(4);
	}

	sendaddr.sin_port = htons(sendport);
	for (;;) {
		numRead = read(inotifyFd, buf, BUF_LEN);
		if (numRead == 0 || numRead == -1)
			perror("read() from inotify fd returned 0 or -1!");

		for (p = buf; p < buf + numRead; ) {
			event = (struct inotify_event *) p;
			numSent = sendto(sockfd, MESSAGE, sizeof(MESSAGE), 0,
			                 (struct sockaddr *)&sendaddr, 
			                 sizeof(sendaddr));
			if(numSent < 0) {
				perror("sendto()");
			}
			p += sizeof(struct inotify_event) + event->len;
		}
	}
}

